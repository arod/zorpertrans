package com.zorperan.transit;

import org.bukkit.util.Vector;

public enum CardinalDirection {
    N(new Vector(0, 0, -1)),
    E(new Vector(1, 0, 0)),
    S(new Vector(0, 0, 1)),
    W(new Vector(-1, 0, 0));

    private final Vector _dir;

    CardinalDirection(Vector dir) {
        _dir = dir;
    }

    Vector getVec() {
        return _dir;
    }

    public static CardinalDirection fromString(String s) {
        for (CardinalDirection d : CardinalDirection.values()) {
            if (s.equalsIgnoreCase(d.name())) return d;
        }
        return null;
    }

    public static CardinalDirection fromVec(Vector v) {
        // Above z=x
        boolean test1 = v.getZ() > v.getX();
        // Above z=-x
        boolean test2 = v.getZ() > -v.getX();

        if (test1) {
            return test2 ? S : W;
        }
        else {
            return test2 ? E : N;
        }
    }

    public static CardinalDirection rotateCW(CardinalDirection dir) {
        switch (dir) {
            case N: return E;
            case E: return S;
            case S: return W;
            case W: return N;
            default: return null;
        }
    }

    public static CardinalDirection rotateCCW(CardinalDirection dir) {
        switch (dir) {
            case N: return W;
            case E: return N;
            case S: return E;
            case W: return S;
            default: return null;
        }
    }
}
