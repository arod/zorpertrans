package com.zorperan.transit;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class CartMeta {
    public Location last_update_loc;
    public Vector storage_ofs;
    public Integer storage_radius;
    public String target_station;
    public int[] route;
}
