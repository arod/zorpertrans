package com.zorperan.transit;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.configuration.Configuration;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Config {
    public enum Function {
        PLATFORM,
        SPAWNER,
        DESTROYER,
        ACTIONER,
        ROUTER,
        PORTALER
    }

    public boolean always_max_speed;
    public int platform_pickup_radius;
    public int default_max_speed_pct;
    public int default_storage_radius;
    public String route_graph_filename;

    private List<Map.Entry<Material, Function>> _controlBlocks = new ArrayList<>();
    private Configuration _plugConfig;

    Config(ZorperTrans plugin) {
        _plugConfig = plugin.getConfig();

        platform_pickup_radius = init("main.platform_pickup_radius", 2);
        default_max_speed_pct = init("main.default_max_speed_pct", 125);
        default_storage_radius = init("main.default_storage_radius", 1);
        route_graph_filename = init("main.route_graph_filename", "route_graph.yaml");

        initFunction(Function.PLATFORM, "blocks.platform", "minecraft:white_wool");
        initFunction(Function.SPAWNER, "blocks.spawner", "minecraft:green_wool");
        initFunction(Function.DESTROYER, "blocks.destroyer", "minecraft:red_wool");
        initFunction(Function.ACTIONER, "blocks.actioner", "minecraft:.*planks");
        initFunction(Function.ROUTER, "blocks.router", "minecraft:.*log");
        initFunction(Function.PORTALER, "blocks.portaler", "minecraft:glowstone");
    }

    public Function getFunction(Material mat) {
        for (Map.Entry<Material, Function> entry : _controlBlocks) {
            if (entry.getKey() == mat) return entry.getValue();
        }
        return null;
    }

    public Config.Function getUnderFunc(Location loc) {
        Block underblk = loc.getBlock().getRelative(0, -1, 0);
        return getFunction(underblk.getType());
    }

    public List<Material> getMaterials(Function func) {
        List<Material> result = new ArrayList<>();
        for (Map.Entry<Material, Function> entry : _controlBlocks) {
            if (entry.getValue() == func) result.add(entry.getKey());
        }
        return result;
    }

    private <T> T init(String path, T def) {
        T val = (T)_plugConfig.get(path, null);
        if (val == null) {
            _plugConfig.set(path, def);
            val = def;
        }
        return val;
    }

    private List<Material> parseMaterialSpec(String str) {
        List<Material> result = new ArrayList<>();
        Pattern pat = Pattern.compile(str);

        for (Material mat : Material.values()) {
            NamespacedKey nskey = mat.getKey();
            Matcher match = pat.matcher(nskey.toString());
            if (match.matches()) result.add(mat);
        }

        return result;
    }

    private void initFunction(Function func, String config_path, String def) {
        String mats_str = init(config_path, def);
        List<Material> mats = parseMaterialSpec(mats_str);
        if (mats.isEmpty()) {
            // do a thing
        }

        for (Material mat : mats) {
            Function exist_func = getFunction(mat);
            if (exist_func != null) {
                // do another thing
            }
            _controlBlocks.add(new AbstractMap.SimpleEntry<>(mat, func));
        }
    }
}
