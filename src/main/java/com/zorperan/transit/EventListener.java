package com.zorperan.transit;

import com.zorperan.transit.routing.RoutingGraph;
import com.zorperan.transit.routing.conditions.RoutingConditionNode;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Rail;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleEntityCollisionEvent;
import org.bukkit.event.vehicle.VehicleMoveEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

public class EventListener implements Listener {
    private ZorperTrans _plugin;
    private Ops _ops;

    EventListener(ZorperTrans plugin) {
        _plugin = plugin;
        _ops = plugin.ops;
    }

    private boolean cartOnRails(Minecart cart) {
        BlockData blk = cart.getLocation().getBlock().getBlockData();
        return (blk instanceof Rail);
    }

    private boolean doSkipMovementUpdateCheck(Minecart cart) {
        final String LOC_KEY = "lastloc";

        long curBlockKey = cart.getLocation().toBlockKey();

        if (cart.hasMetadata(LOC_KEY)) {
            List<MetadataValue> vals = cart.getMetadata(LOC_KEY);
            for (MetadataValue v : vals) {
                long lastBlockKey = v.asLong();
                if (lastBlockKey == curBlockKey) return true;
            }
        }

        cart.setMetadata(LOC_KEY, new FixedMetadataValue(_plugin, curBlockKey));
        return false;
    }

    @EventHandler
    public void onVehicleMove(VehicleMoveEvent event) {
        Vehicle vehicle = event.getVehicle();

        if (!(vehicle instanceof Minecart)) return;
        Minecart minecart = (Minecart)vehicle;
        if (!_ops.isZorpCart(minecart)) return;
        if (!cartOnRails(minecart)) return;

        if (!event.getTo().getWorld().equals(event.getFrom().getWorld())) return;

        Location cartBlockLoc = event.getTo().toBlockLocation();
        Location lastBlockLoc = event.getFrom().toBlockLocation();

        // Apply boost
        _ops.boostMinecart(minecart, event.getFrom(), event.getTo());

        if (cartBlockLoc.distanceSquared(lastBlockLoc) < 1) return;

        // Now do expensive stuff

        //ZorperTrans.logger.log(Level.INFO, "Trigger 1 at " + cartBlockLoc);

        // We've entered a new block since last update.
        // Have we also entered a new chunk? Do even more expensive stuff when that happens.
        Chunk lastChunk = lastBlockLoc.getChunk();
        Chunk curChunk = cartBlockLoc.getChunk();
        if (curChunk.getChunkKey() != lastChunk.getChunkKey()) {
            //ZorperTrans.logger.info("New chunk " + curChunk.getX() + " " + curChunk.getZ());
            lastChunk.removePluginChunkTicket(_plugin);
            curChunk.addPluginChunkTicket(_plugin);
        }

        //ZorperTrans.logger.info("Trigger 2 at " + cartBlockLoc);
        int[] destLoc = {cartBlockLoc.getBlockX(), cartBlockLoc.getBlockY(), cartBlockLoc.getBlockZ()};
        int[] curLoc = {lastBlockLoc.getBlockX(), lastBlockLoc.getBlockY(), lastBlockLoc.getBlockZ()};
        int[] step = Util.calcStepBetweenLocs(curLoc, destLoc);

        //ZorperTrans.logger.log(Level.INFO, " curLoc is " + curLoc[0] + " " + curLoc[1] + " " + curLoc[2]);
        //ZorperTrans.logger.log(Level.INFO, " destLoc is " + destLoc[0] + " " + destLoc[1] + " " + destLoc[2]);
        //ZorperTrans.logger.log(Level.INFO, " step is " + step[0] + " " + step[1] + " " + step[2]);

        // Don't handle x/z diagonals
        if (step[0] != 0 && step[2] != 0) {
            curLoc[0] = destLoc[0];
            curLoc[1] = destLoc[1];
            curLoc[2] = destLoc[2];
        }

        // Interpolation loop
        do {
            if (curLoc[0] != destLoc[0]) curLoc[0] += step[0];
            if (curLoc[1] != destLoc[1]) curLoc[1] += step[1];
            if (curLoc[2] != destLoc[2]) curLoc[2] += step[2];
            //ZorperTrans.logger.info(" Update at " + curLoc[0] + " " + curLoc[1] + " " + curLoc[2] + " # " + event.getTo().toVector());
            boolean destroyed = _ops.handleMovementCtrlBlocks(
                    new Location(cartBlockLoc.getWorld(), curLoc[0], curLoc[1], curLoc[2]), minecart);
            if (destroyed) return;
            //ZorperTrans.logger.log(Level.INFO, " destLoc is " + destLoc[0] + " " + destLoc[1] + " " + destLoc[2]);
        } while (curLoc[0] != destLoc[0] || curLoc[1] != destLoc[1] || curLoc[2] != destLoc[2]);
    }

    @EventHandler
    public void onVehicleDamage(VehicleDamageEvent event) {
        if (_ops.isZorpCart(event.getVehicle()) && event.getAttacker() instanceof HumanEntity) {
            _ops.removeCart(event.getVehicle());
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onVehicleDestroy(VehicleDestroyEvent event) {
        if (_ops.isZorpCart(event.getVehicle()) && event.getAttacker() instanceof HumanEntity) {
            _ops.removeCart(event.getVehicle());
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onSignDestroy(BlockBreakEvent event) {
        BlockState bs = event.getBlock().getState();
        if (!(bs instanceof Sign)) return;

        SignParser.Result parsed = new SignParser.Result();
        SignParser.visitSign((Sign)bs, parsed);

        if (parsed.node_id != null) {
            int node_id = parsed.node_id;
            RoutingGraph graph = _plugin.rgraph;
            graph.remove(node_id);

            event.getPlayer().sendMessage("Removed node " + node_id);
        }
    }

    @EventHandler
    public void onSignEdit(SignChangeEvent event) {
        Sign sign = (Sign)event.getBlock().getState();
        Player player = event.getPlayer();

        SignParser.Result params = new SignParser.Result();
        SignParser.visitLines(event.getLines(), params);

        RoutingGraph graph = _plugin.rgraph;
        Integer node_id = null;

        if (params.alloc_node_id_line != null) {
            node_id = graph.nextFree();
            player.sendMessage("Allocated new node id " + node_id);

            // Edit the sign
            event.setLine(params.alloc_node_id_line, "n:" + node_id);
        }
        else if (params.node_id != null) {
            node_id = params.node_id;
        }

        // Allocated or explicitly specified
        if (node_id != null) {
            RoutingGraph.Node node = new RoutingGraph.Node(node_id);

            if (params.station_def != null) node.station = params.station_def.getStation();
            node.location = sign.getLocation().toBlockLocation();

            if (params.neighbour_defs != null) {
                for (RoutingConditionNode neigh_def : params.neighbour_defs) {
                    node.fanout.put(neigh_def.getNextId(), neigh_def.getCost());
                }
            }

            boolean exists = graph.exists(node_id);
            graph.setOrUpdate(node);
            player.sendMessage((exists? "Updating " : "Creating new ") + "node " + node_id);
        }
    }

    @EventHandler
    public void onRoadkill(VehicleEntityCollisionEvent event) {
        Vehicle vehicle = event.getVehicle();
        if (!(vehicle instanceof Minecart) || !_ops.isZorpCart(vehicle)) return;
        if (vehicle.getVelocity().lengthSquared() == 0) return;
        Entity entity = event.getEntity();
        if (entity instanceof Mob) {
            Mob mob = (Mob)entity;
            mob.damage(mob.getHealth() * 2.0);
            event.setCollisionCancelled(true);
        }
    }

    @EventHandler
    public void onRedstone(BlockRedstoneEvent event) {
        // Activate on posedge only
        if (event.getOldCurrent() > 0 || event.getNewCurrent() == 0) {
            return;
        }

        Block this_block = event.getBlock();
        if (this_block.getType() != Material.POWERED_RAIL)
            return;

        Config.Function func = _plugin.config.getUnderFunc(this_block.getLocation());
        if (func != null) {
            List<Sign> signs = _ops.getSignsInRadius(this_block.getLocation(), 1);
            SignParser.Result params = SignParser.parseSigns(signs);

            switch (func) {
                case SPAWNER:
                    _ops.spawnMinecart(this_block, params);
                    break;
            }
        }
    }
}