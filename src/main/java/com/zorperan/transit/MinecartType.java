package com.zorperan.transit;

import org.bukkit.entity.Minecart;
import org.bukkit.entity.minecart.RideableMinecart;
import org.bukkit.entity.minecart.StorageMinecart;

public enum MinecartType {
    NORMAL(RideableMinecart.class),
    STORAGE(StorageMinecart.class);

    private final Class<? extends Minecart> _class;

    MinecartType(Class<? extends Minecart> clazz) {
        _class = clazz;
    }

    public Class<? extends Minecart> cartClass() {
        return _class;
    }

    public static MinecartType fromString(String s) {
        for (MinecartType e : MinecartType.values()) {
            if (s.equalsIgnoreCase(e.name())) return e;
        }
        return null;
    }
}
