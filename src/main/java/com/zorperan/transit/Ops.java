package com.zorperan.transit;

import com.zorperan.transit.routing.RoutingGraph;
import com.zorperan.transit.routing.conditions.RoutingCondition;
import com.zorperan.transit.routing.conditions.RoutingConditionEmpty;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Rail;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.minecart.RideableMinecart;
import org.bukkit.inventory.BlockInventoryHolder;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Ops {
    private final ZorperTrans _plugin;
    private final String PLAYER_STATION = "station";

    Ops(ZorperTrans plugin) {
        _plugin = plugin;
    }

    public Vector getLaunchDirection(Block at_block) {
        BlockData bd = at_block.getBlockData();
        assert(bd instanceof Rail);
        Rail rail = (Rail)bd;

        Vector back;
        Vector fwd;

        switch(rail.getShape()) {
            case ASCENDING_EAST:
                fwd = new Vector(1, 1, 0);
                back = new Vector(-1, -1, 0);
                break;
            case ASCENDING_WEST:
                fwd = new Vector(-1, 1, 0);
                back = new Vector(1, -1, 0);
                break;
            case ASCENDING_SOUTH:
                fwd = new Vector(0, 1, 1);
                back = new Vector(0, -1, -1);
                break;
            case ASCENDING_NORTH:
                fwd = new Vector(0, -1, 1);
                back = new Vector(0, 1, -1);
                break;
            case EAST_WEST:
                fwd = new Vector(1, 0, 0);
                back = new Vector(-1, 0, 0);
                break;
            case NORTH_SOUTH:
                fwd = new Vector(0, 0, 1);
                back = new Vector(0, 0, -1);
                break;
            case NORTH_EAST:
                fwd = new Vector(1, 0, 0);
                back = new Vector(0, 0, -1);
                break;
            case NORTH_WEST:
                fwd = new Vector(-1, 0, 0);
                back = new Vector(0, 0, -1);
                break;
            case SOUTH_EAST:
                fwd = new Vector(1, 0, 0);
                back = new Vector(0, 0, 1);
                break;
            case SOUTH_WEST:
                fwd = new Vector(-1, 0, 0);
                back = new Vector(0, 0, 1);
                break;
            default:
                fwd = new Vector(0,0,0);
                back = new Vector(0,0,0);
        }

        Block fwd_block = at_block.getRelative(fwd.getBlockX(), fwd.getBlockY(), fwd.getBlockZ());
        Block back_block= at_block.getRelative(back.getBlockX(), back.getBlockY(), back.getBlockZ());

        if (fwd_block.getBlockData() instanceof Rail) return fwd;
        else if (back_block.getBlockData() instanceof Rail) return back;
        else return new Vector(0,0,0);
    }

    public static double calcSpeedFromPct(int pct) {
        return 0.4 * (double)pct / 100.0;
    }

    public boolean handleMovementCtrlBlocks(Location loc, Minecart minecart) {
        Config.Function func = _plugin.config.getUnderFunc(loc);
        boolean destroyed = false;
        if (func != null) {
            CartMeta cartmeta = deserializeZorpCart(minecart);
            List<Sign> signs = getSignsInRadius(loc, 1);
            SignParser.Result params = SignParser.parseSigns(signs);

            //ZorperTrans.logger.log(Level.INFO, "Ctrlblock " + func + " at " + loc + " #signs " + signs.size());

            switch(func) {
                case ACTIONER:
                    doActioner(loc, minecart, cartmeta, params);
                    break;

                case PLATFORM:
                    doPlatform(loc, minecart, cartmeta);
                    break;

                case DESTROYER:
                    doDestroyer(loc, minecart, params);
                    destroyed = true;
                    break;

                case ROUTER:
                    destroyed = doRouter(loc, minecart, cartmeta, params);
                    break;

                case PORTALER:
                    doPortaler(loc, minecart, cartmeta, params);
                    break;
            }

            updateZorpCart(minecart, cartmeta);
        }

        return destroyed;
    }

    private void doPortaler(Location loc, Minecart minecart, CartMeta cartmeta, SignParser.Result params) {
        //ZorperTrans.logger.info("MS 0");
        if (params.portal_target == null) return;

        //ZorperTrans.logger.info("MS 1");

        // Get speed and dir of cart
        Vector cart_vel = minecart.getVelocity();
        if (cart_vel.lengthSquared() == 0) return;

        Vector cart_dir = cart_vel.normalize();

        // Check for portal surface ahead
        Location portalLoc = loc.clone().add(cart_dir);
        Block aheadBlock = portalLoc.getBlock();
        //ZorperTrans.logger.info("MS 2 " + aheadBlock.getLocation() + ":" + aheadBlock.getType());
        if (aheadBlock.getType() != Material.NETHER_PORTAL) return;

        // Find out what dimension we're in and which one we'll go to
        World this_world = loc.getWorld();
        World.Environment targ_env = null;
        float scale_fac = 0;
        switch (this_world.getEnvironment()) {
            case NORMAL: targ_env = World.Environment.NETHER; scale_fac = 0.125f; break;
            case NETHER: targ_env = World.Environment.NORMAL; scale_fac = 8.0f; break;
            default: return;
        }

        //ZorperTrans.logger.info("MS 3 " + targ_env);

        // Do a coordinate check
        if (Math.abs(params.portal_target.getX() - portalLoc.getX() * scale_fac) > 128) return;
        if (Math.abs(params.portal_target.getZ() - portalLoc.getZ() * scale_fac) > 128) return;

        //ZorperTrans.logger.info("MS 4 ");

        // Get the World with the matching environment
        World targ_world = null;
        for (World world : _plugin.getServer().getWorlds()) {
            if (world.getEnvironment() == targ_env) {
                targ_world = world;
                break;
            }
        }

        //ZorperTrans.logger.info("MS 5 " + targ_world);

        if (targ_world == null) return;

        Location targ_loc = new Location(targ_world, params.portal_target.getX(),
                params.portal_target.getY(), params.portal_target.getZ());

        loc.getChunk().removePluginChunkTicket(_plugin);
        targ_loc.getChunk().addPluginChunkTicket(_plugin);

        List<Entity> passengers = minecart.getPassengers();
        double max_speed = minecart.getMaxSpeed();  // gets reset after teleport

        for (Entity passenger : passengers) {
            //ZorperTrans.logger.info("Passenger " + passenger);
            minecart.removePassenger(passenger);
        }
        this_world.playEffect(loc, Effect.ENDER_SIGNAL, 0);
        this_world.playSound(loc, Sound.ENTITY_ENDERMAN_TELEPORT, 1, 1.25f);
        minecart.teleport(targ_loc.toCenterLocation());
        targ_world.playEffect(targ_loc, Effect.ENDER_SIGNAL, 0);
        targ_world.playSound(targ_loc, Sound.ENTITY_ENDERMAN_TELEPORT, 1, 1.25f);

        minecart.setMaxSpeed(max_speed);
        minecart.setVelocity(cart_vel);

        _plugin.getServer().getScheduler().runTask(_plugin, new Runnable() {
            @Override
            public void run() {
                for (Entity passenger : passengers) {
                    passenger.teleport(targ_loc);
                    minecart.addPassenger(passenger);
                }
            }
        });
    }

    public void spawnMinecart(Block at_block, SignParser.Result params) {
        World world = at_block.getWorld();

        // Don't spawn minecarts on top of each other
        Collection<Minecart> existCarts =
                world.getNearbyEntitiesByType(Minecart.class, at_block.getLocation(), 1);
        if (!existCarts.isEmpty())
            return;

        // Determine cart type and direction
        Class<? extends Minecart> cart_class = params.spawn_type == null ?
                RideableMinecart.class : params.spawn_type.cartClass();

        Vector spawn_vel = params.spawn_dir == null ?
                getLaunchDirection(at_block) : params.spawn_dir.getVec();

        Minecart mc = world.spawn(at_block.getLocation().toCenterLocation(), cart_class);
        mc.setMaxSpeed(calcSpeedFromPct(_plugin.config.default_max_speed_pct));
        mc.setSlowWhenEmpty(false);

        CartMeta meta = new CartMeta();
        applyCartProperties(mc, meta, params);

        if (spawn_vel.lengthSquared() > 0) {
            spawn_vel = spawn_vel.multiply(mc.getMaxSpeed());
        }
        mc.setVelocity(spawn_vel);

        updateZorpCart(mc, meta);
    }

    public void boostMinecart(Minecart minecart, Location from, Location to) {
        Vector true_vel = minecart.getVelocity();
        Vector alt_vel = to.subtract(from).toVector();

        //ZorperTrans.logger.info("BOOST " + minecart.getLocation().toBlockLocation().toVector() + " # " + minecart.getLocation().toVector());

        if (true_vel.lengthSquared() > 0 || alt_vel.lengthSquared() > 0) {
            Rail rail = (Rail)minecart.getLocation().toBlockLocation().getBlock().getBlockData();
            Vector cur_vel = alt_vel;
            switch (rail.getShape()) {
                case ASCENDING_EAST: cur_vel.setY(cur_vel.getX()); break;
                case ASCENDING_NORTH: cur_vel.setY(-cur_vel.getZ()); break;
                case ASCENDING_SOUTH: cur_vel.setY(cur_vel.getZ()); break;
                case ASCENDING_WEST: cur_vel.setY(-cur_vel.getX()); break;
                default: cur_vel = true_vel; break;
            }

            //ZorperTrans.logger.info((slope? "  Slope " : "        ")  + minecart.getLocation().toVector());

            //ZorperTrans.logger.info("From " + from + " To " + to + " Cur vel: " + cur_vel);
            if (cur_vel.lengthSquared() > 0) {
                Vector new_vel = cur_vel.normalize().multiply(minecart.getMaxSpeed());
                //ZorperTrans.logger.info("  New vel: " + new_vel);
                minecart.setVelocity(new_vel);
            }
        }
    }

    public boolean doRouter(Location loc, Minecart minecart, CartMeta meta, SignParser.Result params) {
        // Is this router block also a routing node?
        if (params.node_id != null) {
            RoutingGraph graph = _plugin.rgraph;
            int this_node = params.node_id;

            if (meta.route == null && meta.target_station != null) {
                Integer station_node = graph.getIdForStation(meta.target_station);
                if (station_node != null) {
                    meta.route = graph.findShortestPath(this_node, station_node);
                }
            }

            if (meta.route != null) {
                // yes route but head(route) not match node id?
                if (meta.route.length == 0 || meta.route[0] != this_node) {
                    // print some error
                    meta.route = null;
                }
                else if (meta.route.length == 1) {
                    // success arrived
                    meta.route = null;
                }
                else {
                    // successfully consume this routing id from the route
                    meta.route = Arrays.copyOfRange(meta.route, 1, meta.route.length);
                }
            }

            updateZorpCart(minecart, meta);
        }

        return applyRoutingConditions(loc, minecart, meta, params);
    }

    private boolean destroyRideableCartIfEmpty(Minecart minecart) {
        if (minecart instanceof  RideableMinecart && minecart.getPassengers().isEmpty()) {
            removeCart(minecart);
            return true;
        }
        return false;
    }

    private boolean applyRoutingConditions(Location loc, Minecart minecart, CartMeta meta, SignParser.Result params) {
        if (params.route_conds == null) return destroyRideableCartIfEmpty(minecart);

        CardinalDirection newDir = null;
        boolean hasExplicitEmptyCondition = false;
        for (RoutingCondition cond : params.route_conds) {
            // Use only first matching condition for direction
            if (newDir == null && cond.eval(minecart, meta)) {
                newDir = cond.getDir();
            }

            hasExplicitEmptyCondition = cond instanceof RoutingConditionEmpty;
        }

        // Id-based routing nodes need an explicit rc:empty node condition,
        // otherwise empty passenger carts get nuked for safety
        if (!hasExplicitEmptyCondition && params.node_id != null && destroyRideableCartIfEmpty(minecart)) {
            return true; // destroyed
        }

        if (newDir != null) {
            BlockState bs = loc.getBlock().getState();
            BlockData bd = bs.getBlockData();
            if (bd instanceof Rail) {
                Rail rail = (Rail)bd;
                CardinalDirection curDir = CardinalDirection.fromVec(minecart.getVelocity());
                Rail.Shape curShape = rail.getShape();
                Rail.Shape newShape = RailShapeMap.lookup(curShape, curDir, newDir);
                if (newShape != null && newShape != curShape) {
                    //ZorperTrans.logger.log(Level.INFO, newShape.toString());
                    rail.setShape(newShape);
                    bs.setBlockData(rail);
                    bs.update();

                    minecart.getWorld().playSound(loc, Sound.BLOCK_ANVIL_LAND, 0.25f, 1);
                }

                if (newDir != curDir) {
                    Vector curVel = minecart.getVelocity();
                    Vector newVel = newDir.getVec().multiply(curVel.length());
                    minecart.setVelocity(newVel);
                }
            }
        }

        return false; // not destroyed
    }

    public void applyCartProperties(Minecart minecart, CartMeta meta, SignParser.Result params) {
        // Apply change max speed command
        if (params.speed != null) {
            minecart.setMaxSpeed(Ops.calcSpeedFromPct(params.speed));
        }

        // Apply storage properties
        if (params.storage_ofs != null) {
            meta.storage_ofs = params.storage_ofs;
            //ZorperTrans.logger.log(Level.INFO, "Ofs " + meta.storage_ofs);
        }
        if (params.storage_radius != null) {
            meta.storage_radius = params.storage_radius;
            //ZorperTrans.logger.log(Level.INFO, "Radius " + meta.storage_radius);
        }

        // Station override
        if (params.set_station != null) {
            meta.target_station = params.set_station;
            meta.route = null;
        }
    }

    public void doActioner(Location loc, Minecart minecart, CartMeta meta, SignParser.Result params) {
        applyCartProperties(minecart, meta, params);

        // Execute storage directives
        //ZorperTrans.logger.log(Level.INFO, "  sget is " + (params.get_spec != null));
        if (minecart instanceof InventoryHolder && (params.get_spec != null || params.put_spec != null)) {
            BlockInventoryHolder chestInvHolder = null;
            if (meta.storage_ofs != null) chestInvHolder = getBlkInvAtOffset(loc, meta.storage_ofs);
            //ZorperTrans.logger.log(Level.INFO, "  chest is  " + (chestInvHolder != null));
            if (chestInvHolder == null) {
                int radius = _plugin.config.default_storage_radius;
                if (meta.storage_radius != null) radius = meta.storage_radius;
                chestInvHolder = getBlkInvInRadius(loc, radius);
            }

            if (chestInvHolder != null) {
                Inventory chest_inv = chestInvHolder.getInventory();
                Inventory cart_inv = ((InventoryHolder) minecart).getInventory();

                if (params.get_spec != null) {
                    params.get_spec.transfer(chest_inv, cart_inv);
                }
                if (params.put_spec != null) {
                    params.put_spec.transfer(cart_inv, chest_inv);
                }
            }
        }
    }

    public void doDestroyer(Location loc, Minecart minecart, SignParser.Result params) {
        Location dropoffLoc = loc.clone();
        dropoffLoc.setDirection(minecart.getLocation().getDirection());

        if (params.dropoff_ofs != null) {
            dropoffLoc.add(params.dropoff_ofs);
        }

        List<Entity> passengers = minecart.getPassengers();
        minecart.eject();
        removeCart(minecart);
        for (Entity ent : passengers) {
            ent.teleport(dropoffLoc.toCenterLocation());
        }
    }

    public void doPlatform(Location loc, Minecart minecart, CartMeta meta) {
        if (!(minecart instanceof RideableMinecart) || !minecart.getPassengers().isEmpty())
            return;

        int radius = _plugin.config.platform_pickup_radius;
        Collection<LivingEntity> ents = minecart.getWorld().getNearbyEntitiesByType(
                LivingEntity.class, loc, radius);

        for (LivingEntity ent : ents) {
            if (ent instanceof HumanEntity) {
                String station = getPlayerStation((HumanEntity)ent);
                if (station != null) {
                    meta.target_station = station;
                    meta.route = null;
                }
            }
            minecart.addPassenger(ent);
            break;
        }
    }

    public boolean isZorpCart(Entity e) {
        PersistentDataContainer cont = e.getPersistentDataContainer();
        return cont.has(_plugin.sers.cart.key, _plugin.sers.cart);
    }

    public CartMeta deserializeZorpCart(Entity e) {
        PersistentDataContainer cont = e.getPersistentDataContainer();
        return cont.get(_plugin.sers.cart.key, _plugin.sers.cart);
    }

    public void updateZorpCart(Minecart e, CartMeta meta) {
        PersistentDataContainer cont = e.getPersistentDataContainer();
        cont.set(_plugin.sers.cart.key, _plugin.sers.cart, meta);
    }

    public void removeCart(Entity cart) {
        cart.getChunk().removePluginChunkTicket(_plugin);
        cart.remove();;
    }

    public List<Sign> getSignsInRadius(Location loc, int rad) {
        List<BlockState> result = getBlocksInRadius(loc, Sign.class, rad, false);
        return (List)result;
    }

    public BlockInventoryHolder getBlkInvAtOffset(Location loc, Vector offset) {
        Location probeLoc = loc.add(offset);
        BlockState bs = probeLoc.getBlock().getState();
        return (bs instanceof BlockInventoryHolder) ? (BlockInventoryHolder)bs : null;
    }

    public BlockInventoryHolder getBlkInvInRadius(Location loc, int rad) {
        List<BlockState> result = getBlocksInRadius(loc, BlockInventoryHolder.class, rad,true);
        for (BlockState h : result) {
            return (BlockInventoryHolder)h;
        }
        return null;
    }

    public Sign getFirstSignInRadius(Location loc, int rad) {
        List<BlockState> result =  getBlocksInRadius(loc, Sign.class, rad, true);
        for (BlockState b : result) {
            return (Sign)b;
        }
        return null;
    }

    public <T extends BlockState> List<T> getBlocksInRadius(Location loc,
                                                            Class cls,
                                                            int radius,
                                                            boolean stopAtFirst) {
        ArrayList<T> result = new ArrayList<>();
        Location newLoc = loc.clone();

        for (int z = -radius; z <= radius; z++) {
            newLoc.setZ(z + loc.getBlockZ());
            for (int y = -radius; y <= radius; y++) {
                newLoc.setY(y + loc.getBlockY());
                for (int x = -radius; x <= radius; x++) {
                    newLoc.setX(x + loc.getBlockX());
                    BlockState blk = newLoc.getBlock().getState();
                    if (cls.isInstance(blk)) {
                        result.add((T)blk);
                        if (stopAtFirst) return result;
                    }
                }
            }
        }

        return result;
    }

    public void setPlayerStation(HumanEntity player, String station) {
        // Make sure station exists, else post error
        RoutingGraph graph = _plugin.rgraph;

        if (graph.getIdForStation(station) == null) {
            player.sendMessage("Unknown station: " + station);
            return;
        }
        else {
            player.sendMessage("Target station: " + station);
        }

        // Set the value
        player.setMetadata(PLAYER_STATION, new FixedMetadataValue(_plugin, station));
    }

    public void clearPlayerStation(HumanEntity player) {
        player.removeMetadata(PLAYER_STATION, _plugin);
    }

    public String getPlayerStation(HumanEntity player) {
        List<MetadataValue> vals = player.getMetadata(PLAYER_STATION);
        for (MetadataValue val : vals) {
            return val.asString();
        }
        return null;
    }
}
