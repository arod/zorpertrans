package com.zorperan.transit;

import org.bukkit.block.data.Rail;

import java.util.ArrayList;
import java.util.EnumMap;

public class RailShapeMap {
    private static class Entry {
        CardinalDirection in;
        CardinalDirection out;
        Rail.Shape next;

        Entry(CardinalDirection in_, CardinalDirection out_, Rail.Shape next_) {
            in = in_;
            out = out_;
            next = next_;
        }
    }
    
    private static final EnumMap<Rail.Shape, ArrayList<Entry>> _entries;
    
    private static void add(Rail.Shape prev, CardinalDirection in, CardinalDirection out, Rail.Shape next) {
        ArrayList<Entry> list = _entries.getOrDefault(prev, null);
        if (list == null) {
            list = new ArrayList<>();
            _entries.put(prev, list);
        }

        list.add(new Entry(in, out, next));
    }

    public static Rail.Shape lookup(Rail.Shape prev, CardinalDirection in, CardinalDirection out) {
        Rail.Shape key = prev;

        switch (prev) {
            case ASCENDING_EAST:
            case ASCENDING_WEST:
                key = Rail.Shape.EAST_WEST;
                break;
            case ASCENDING_NORTH:
            case ASCENDING_SOUTH:
                key = Rail.Shape.NORTH_SOUTH;
                break;
        }

        ArrayList<Entry> list = _entries.get(key);
        for (Entry entry : list) {
            if (entry.in == in && entry.out == out) {
                return entry.next;
            }
        }

        return null;
    }

    static {
        _entries = new EnumMap<>(Rail.Shape.class);

        add(Rail.Shape.NORTH_WEST, CardinalDirection.W, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.W, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.W, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.W, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.N, CardinalDirection.W, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.N, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.N, CardinalDirection.E, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.N, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.E, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.E, CardinalDirection.N, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.E, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.E, CardinalDirection.S, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.S, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.S, CardinalDirection.N, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.S, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_WEST, CardinalDirection.S, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);

        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.W, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.W, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.W, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.W, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.N, CardinalDirection.W, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.N, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.N, CardinalDirection.E, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.N, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.E, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.E, CardinalDirection.N, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.E, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.E, CardinalDirection.S, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.S, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.S, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.S, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_SOUTH, CardinalDirection.S, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);

        add(Rail.Shape.NORTH_EAST, CardinalDirection.W, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.W, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.W, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.W, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.N, CardinalDirection.W, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.N, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.N, CardinalDirection.E, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.N, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.E, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.E, CardinalDirection.N, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.E, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.E, CardinalDirection.S, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.S, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.S, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.S, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.NORTH_EAST, CardinalDirection.S, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);

        add(Rail.Shape.EAST_WEST, CardinalDirection.W, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.W, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.W, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.W, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.N, CardinalDirection.W, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.N, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.EAST_WEST, CardinalDirection.N, CardinalDirection.E, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.N, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.EAST_WEST, CardinalDirection.E, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.E, CardinalDirection.N, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.E, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.E, CardinalDirection.S, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.S, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.S, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.EAST_WEST, CardinalDirection.S, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.EAST_WEST, CardinalDirection.S, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);

        add(Rail.Shape.SOUTH_EAST, CardinalDirection.W, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.W, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.W, CardinalDirection.E, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.W, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.N, CardinalDirection.W, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.N, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.N, CardinalDirection.E, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.N, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.E, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.E, CardinalDirection.N, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.E, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.E, CardinalDirection.S, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.S, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.S, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.S, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.SOUTH_EAST, CardinalDirection.S, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);

        add(Rail.Shape.SOUTH_WEST, CardinalDirection.W, CardinalDirection.W, Rail.Shape.EAST_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.W, CardinalDirection.N, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.W, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.W, CardinalDirection.S, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.N, CardinalDirection.W, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.N, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.N, CardinalDirection.E, Rail.Shape.SOUTH_EAST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.N, CardinalDirection.S, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.E, CardinalDirection.W, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.E, CardinalDirection.N, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.E, CardinalDirection.E, Rail.Shape.EAST_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.E, CardinalDirection.S, Rail.Shape.SOUTH_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.S, CardinalDirection.W, Rail.Shape.NORTH_WEST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.S, CardinalDirection.N, Rail.Shape.NORTH_SOUTH);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.S, CardinalDirection.E, Rail.Shape.NORTH_EAST);
        add(Rail.Shape.SOUTH_WEST, CardinalDirection.S, CardinalDirection.S, Rail.Shape.NORTH_SOUTH);
    }
}
