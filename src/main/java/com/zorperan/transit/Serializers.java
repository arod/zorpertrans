package com.zorperan.transit;

import com.zorperan.transit.serial.CartMetaSerializer;
import com.zorperan.transit.serial.LocationSerializer;
import com.zorperan.transit.serial.VectorSerializer;

public class Serializers {
    public CartMetaSerializer cart;
    public LocationSerializer location;
    public static final VectorSerializer vector = new VectorSerializer();

    Serializers(ZorperTrans plugin) {
        cart = new CartMetaSerializer(plugin);
        location = new LocationSerializer(plugin);
    }
}
