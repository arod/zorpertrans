package com.zorperan.transit;

import com.zorperan.transit.routing.conditions.RoutingCondition;
import com.zorperan.transit.routing.conditions.RoutingConditionNode;
import com.zorperan.transit.routing.conditions.RoutingConditionStation;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public enum SignCommand {
    TYPE {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                result.spawn_type = MinecartType.fromString(tokens[1]);
            }
        }
    },

    SPAWNDIR {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                result.spawn_dir = CardinalDirection.fromString(tokens[1]);
            }
        }
    },

    SPEED {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                result.speed = Util.parseInt(tokens[1]);
            }
        }
    },

    SGET {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                result.get_spec = StorageTransferSpec.parse(tokens);
            }
        }
    },

    SPUT {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                result.put_spec = StorageTransferSpec.parse(tokens);
            }
        }
    },

    SOFS {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 4) {
                result.storage_ofs = Util.parseVec(tokens[1], tokens[2], tokens[3]);
            }
        }
    },

    SRAD {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                result.storage_radius = Util.parseInt(tokens[1]);
            }
        }
    },

    DROPOFF {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 4) {
                result.dropoff_ofs = Util.parseVec(tokens[1], tokens[2], tokens[3]);
            }
        }
    },

    N {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                if (tokens[1].equals("?")) {
                    result.alloc_node_id_line = lineno;
                }
                else {
                    try {
                        result.node_id = Integer.parseInt(tokens[1]);
                    } catch (NumberFormatException e) {
                    }
                }
            }
        }
    },

    NN {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            RoutingConditionNode rc = RoutingConditionNode.parse(tokens);
            if (rc == null) return;

            if (result.route_conds == null) {
                result.route_conds = new ArrayList<>();
            }
            result.route_conds.add(rc);

            if (result.neighbour_defs == null) {
                result.neighbour_defs = new ArrayList<>();
            }
            result.neighbour_defs.add(rc);
        }
    },

    NS {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            RoutingConditionStation rc = RoutingConditionStation.parse(tokens);
            if (rc == null) return;

            // It's a routing condition
            if (result.route_conds == null) {
                result.route_conds = new ArrayList<>();
            }
            result.route_conds.add(rc);

            // It also has a special meaning on its own
            result.station_def = rc;
        }
    },

    RC {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (result.route_conds == null) {
                result.route_conds = new ArrayList<>();
            }

            RoutingCondition newCond = RoutingCondition.fromString(tokens);
            if (newCond != null) {
                result.route_conds.add(newCond);
            }
        }
    },

    SS {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                result.set_station = tokens[1];
            }
        }
    },

    PX {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                if (result.portal_target == null) result.portal_target = new Vector();
                result.portal_target.setX(Integer.parseInt(tokens[1]));
            }
        }
    },

    PY {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                if (result.portal_target == null) result.portal_target = new Vector();
                result.portal_target.setY(Integer.parseInt(tokens[1]));
            }
        }
    },

    PZ {
        @Override
        public void parse(int lineno, String[] tokens, SignParser.Result result) {
            if (tokens.length == 2) {
                if (result.portal_target == null) result.portal_target = new Vector();
                result.portal_target.setZ(Integer.parseInt(tokens[1]));
            }
        }
    };

    public abstract void parse(int lineno, String[] tokens, SignParser.Result result);
}