package com.zorperan.transit;

import com.zorperan.transit.routing.conditions.RoutingCondition;
import com.zorperan.transit.routing.conditions.RoutingConditionNode;
import com.zorperan.transit.routing.conditions.RoutingConditionStation;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.util.Vector;

import java.util.List;

public class SignParser {
    public static class Result {
        // Spawn
        MinecartType spawn_type;
        CardinalDirection spawn_dir;
        // Cart properties
        Integer speed;
        String set_station;
        // Destroyer
        Vector dropoff_ofs;
        // Storage ops
        StorageTransferSpec put_spec;
        StorageTransferSpec get_spec;
        Vector storage_ofs;
        Integer storage_radius;
        // Routing
        List<RoutingCondition> route_conds;
        // Node Routing
        RoutingConditionStation station_def;
        Integer node_id;
        Integer alloc_node_id_line;
        List<RoutingConditionNode> neighbour_defs;
        // Portals
        Vector portal_target;
    }

    public static Result parseSigns(List<Sign> signs) {
        Result result = new Result();
        for (Sign sign : signs) {
            visitSign(sign, result);
        }
        return result;
    }

    public static void visitSign(Sign sign, Result result) {
        visitLines(sign.getLines(), result);
    }

    public static void visitLines(String[] lines, Result result) {
        int lineno = 0;
        for (String line : lines) {
            if (!line.isEmpty()) visitLine(lineno++, line, result);
        }
    }

    public static void visitLine(int lineno, String line, Result result) {
        String[] tokens = StringUtils.split(line, ": ");

        for (SignCommand e : SignCommand.values()) {
            if (tokens[0].equalsIgnoreCase(e.name())) {
                e.parse(lineno, tokens, result);
                break;
            }
        }
    }
}
