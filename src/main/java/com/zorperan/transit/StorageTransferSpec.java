package com.zorperan.transit;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Map;

public interface StorageTransferSpec {
    enum Type implements StorageTransferSpec {
        ALL {
            @Override
            public void transfer(Inventory src, Inventory dest) {
                ArrayList<ItemStack> putBack = new ArrayList<ItemStack>();
                for (ItemStack srcStack : src.getStorageContents()) {
                    if (srcStack != null) {
                        Map<Integer, ItemStack> addResult = dest.addItem(srcStack);
                        for (ItemStack remainder : addResult.values()) {
                            putBack.add(remainder);
                        }
                    }
                }
                src.clear();
                for (ItemStack remainder : putBack) {
                    src.addItem(remainder);
                }
            }
        },

        SAME {
            @Override
            public void transfer(Inventory src, Inventory dest) {
                ArrayList<ItemStack> putBack = new ArrayList<ItemStack>();
                boolean transferredSomething = false;
                for (ItemStack srcStack : src.getStorageContents()) {
                    if (srcStack != null && dest.contains(srcStack.getType())) {
                        Map<Integer, ItemStack> addResult = dest.addItem(srcStack);
                        transferredSomething = true;
                        for (ItemStack remainder : addResult.values()) {
                            putBack.add(remainder);
                        }
                    } else {
                        // Matched some stacks  but not others. clear() will be called so populate putBack
                        // to avoid this stack being lost.
						putBack.add(srcStack);
					}
                }
                if (transferredSomething) {
                    src.clear();
                    for (ItemStack remainder : putBack) {
                        src.addItem(remainder);
                    }
                }
            }
        }
    }

    void transfer(Inventory src, Inventory dest);

    static StorageTransferSpec parse(String[] tokens) {
        if (tokens.length < 2)
            return null;

        String type = tokens[1];
        for (StorageTransferSpec.Type e : StorageTransferSpec.Type.values()) {
            if (type.equalsIgnoreCase(e.name())) {
                return e;
            }
        }

        return null;
    }
}
