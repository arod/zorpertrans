package com.zorperan.transit;

import org.bukkit.util.Vector;

public class Util {
    public static Integer parseInt(String s) {
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }

    public static Vector parseVec(String x, String y, String z) {
        Integer xx = Util.parseInt(x);
        Integer yy = Util.parseInt(y);
        Integer zz = Util.parseInt(z);
        if (xx != null && yy != null && zz != null) {
            return new Vector(xx, yy, zz);
        }
        return null;
    }

    public static int[] calcStepBetweenLocs(int[] src, int[] dest) {
        int dx = dest[0] - src[0];
        int dy = dest[1] - src[1];
        int dz = dest[2] - src[2];
        if (dx != 0) dx /= Math.abs(dx);
        if (dy != 0) dy /= Math.abs(dy);
        if (dz != 0) dz /= Math.abs(dz);

        int[] result = {dx ,dy ,dz};
        return result;
    }
}
