package com.zorperan.transit;

import com.zorperan.transit.routing.RoutingGraph;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Entry point for the template plugin. You should edit
 * this comment by explaining the main purpose of your
 * plugin
 *
 * You should also edit these tags below.
 *
 * @author arod
 * @version 1.0-SNAPSHOT
 * @since 1.0-SNAPSHOT
 */

// Cart auto-destroy
// Signs
// Storage
// Set cart type on spawn

public class ZorperTrans extends JavaPlugin {
    public static Logger logger;
    public static Server server;

    public Config config;
    public Serializers sers;
    public Ops ops;
    public RoutingGraph rgraph;

    private EventListener _listener;

    @Override
    public void onLoad() {
        ZorperTrans.logger = this.getLogger();
        ZorperTrans.server = this.getServer();
    }

    @Override
    public void onEnable() {
        this.config = new Config(this);
        this.ops = new Ops(this);
        this.sers = new Serializers(this);
        this.rgraph = new RoutingGraph(this);

        this.saveConfig();
        this.rgraph.load();

        PluginManager plugman = server.getPluginManager();
        _listener = new EventListener(this);
        plugman.registerEvents(_listener, this);
    }

    @Override
    public void onDisable() {
        this.rgraph.save();
        this.saveConfig();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equals("st") && args.length == 1 && sender instanceof HumanEntity) {
            String station = args[0];
            this.ops.setPlayerStation((HumanEntity)sender, station);
            return true;
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        if (cmd.getName().equals("st")) {
            String prefix = args.length == 1 ? args[0] : "";
            List<String> allStations = this.rgraph.getStationList();
            return allStations.stream().filter(p -> p.startsWith(prefix)).collect(Collectors.toList());
        }
        return null;
    }
}
