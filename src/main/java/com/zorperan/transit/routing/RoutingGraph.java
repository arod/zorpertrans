package com.zorperan.transit.routing;

import com.zorperan.transit.ZorperTrans;
import org.bukkit.Location;
import org.bukkit.World;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.*;

public class RoutingGraph {
    public static class Node {
        public final int id;
        public String station;
        public HashMap<Integer, Integer> fanout = new HashMap<>();  // k: nodeid, v: cost
        public Location location;
        public Node(int id_) { id = id_;}
    }

    private ArrayList<Node> _nodes = new ArrayList<>();
    private HashMap<String, Integer> _stations = new HashMap<>();
    private ZorperTrans _plugin;
    private final String KEY_STATION = "station";
    private final String KEY_FANOUT = "fanout";
    private final String KEY_LOCATION = "location";

    public RoutingGraph(ZorperTrans plugin) {
        _plugin = plugin;
    }

    public Node get(int id) {
        return (id >= _nodes.size())? null : _nodes.get(id);
    }

    public boolean exists(int id) { return get(id) != null; }

    public Integer getIdForStation(String station) {
        return _stations.getOrDefault(station, null);
    }

    public List<String> getStationList() {
        return new ArrayList<>(_stations.keySet());
    }

    public int nextFree() {
        for (int i = 0; i < _nodes.size(); i++) {
            if (_nodes.get(i) == null) return i;
        }
        return _nodes.size();
    }

    private File getGraphFile() {
        return new File(_plugin.getDataFolder(), _plugin.config.route_graph_filename);
    }

    public void load() {
        File file = getGraphFile();
        Yaml yaml = new Yaml();

        _nodes.clear();
        _stations.clear();

        List<Map> ynl = null;

        try {
            ynl = yaml.load(new FileReader(file));
        } catch (FileNotFoundException e) {
            ZorperTrans.logger.warning("Could not find " + file.getName() + " so using empty graph");
            return;
        }

        if (ynl == null) return;
        _nodes.ensureCapacity(ynl.size());

        for (int id = 0; id < ynl.size(); id++) {
            Map yn = ynl.get(id);
            if (yn == null) continue;

            Node node = new Node(id);

            node.station = (String)yn.getOrDefault(KEY_STATION, null);

            List yloc = (List)yn.get(KEY_LOCATION);
            World world = _plugin.getServer().getWorld((String)yloc.get(0));
            if (world == null) {
                continue;
            }
            node.location = new Location(world, (int)yloc.get(1), (int)yloc.get(2), (int)yloc.get(3));
            node.fanout = (HashMap)yn.getOrDefault(KEY_FANOUT, new HashMap<>());

            setOrUpdate(node);
        }
    }

    public void save() {
        //removeStaleRefs();

        List<Object> ynl = new ArrayList<>();
        for (Node node : _nodes) {
            if (node == null) {
                ynl.add(null);
                continue;
            }

            Map<String, Object> yn = new HashMap<>();

            if (node.station != null) yn.put(KEY_STATION, node.station);

            Object[] yloc = {
                    node.location.getWorld().getName(),
                    node.location.getBlockX(),
                    node.location.getBlockY(),
                    node.location.getBlockZ()
            };
            yn.put(KEY_LOCATION, yloc);

            if (!node.fanout.isEmpty()) {
                yn.put(KEY_FANOUT, node.fanout);
            }

            ynl.add(yn);
        }

        try {
            File file = getGraphFile();
            Yaml yaml = new Yaml();
            yaml.dump(ynl, new FileWriter(file));
        } catch (IOException e) {
            ZorperTrans.logger.warning("Could not write nodes file because " + e.getMessage());
        }
    }

    private void removeStaleRefs() {
        // Removes any fanouts pointing to dead nodes
        for (Node node : _nodes) {
            if (node == null) continue;

            Iterator<Map.Entry<Integer, Integer>> iter = node.fanout.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<Integer, Integer> kv = iter.next();
                int nextId = kv.getKey();
                if (!exists(nextId)) {
                    iter.remove();
                }
            }
        }

        // Removes any station defs pointing to dead nodes
        for (Map.Entry<String, Integer> entry : _stations.entrySet()) {
            int id = entry.getValue();
            if (!exists(id)) {
                _stations.remove(entry.getKey());
            }
        }
    }

    public void remove(int id) {
        Node node = get(id);
        if (node == null) return;

        if (node.station != null) {
            _stations.remove(node.station, id);
        }

        _nodes.set(id, null);
    }

    public void setOrUpdate(Node node) {
        int id = node.id;

        for (int i = _nodes.size(); i <= id; i++) {
            _nodes.add(null);
        }
        remove(id);
        _nodes.set(id, node);
        if (node != null && node.station != null) {
            _stations.put(node.station, id);
        }
    }

    public int[] findShortestPath(int src_id, int dest_id) {
        if (src_id == dest_id) return new int[] {dest_id};

        int N = _nodes.size();
        int[] dist = new int[N];    // distance from src
        int[] prev = new int[N];    // id of best prev node

        Arrays.fill(dist, Integer.MAX_VALUE);
        Arrays.fill(prev, -1);
        dist[src_id] = 0;

        // Contains unvisited nodes sorted by current best cost
        PriorityQueue<Node> to_visit = new PriorityQueue<>(N,
                Comparator.comparingInt((Node node) -> dist[node.id]));
        to_visit.add(get(src_id));

        while (!to_visit.isEmpty()) {
            Node cur_node = to_visit.poll();
            int cur_id = cur_node.id;
            int cur_dist = dist[cur_id];

            if (cur_id == dest_id) continue;

            for (Map.Entry<Integer, Integer> entry : cur_node.fanout.entrySet()) {
                int next_id = entry.getKey();
                Node next_node = get(next_id);

                if (next_node == null) {
                    _plugin.getServer().broadcastMessage(
                            "Node " + cur_id + " specified bad neighbour " + next_id);
                    break;
                }

                int incr_dist = entry.getValue();
                int alt_dist = cur_dist + incr_dist;
                if (alt_dist < dist[next_id]) {
                    dist[next_id] = alt_dist;
                    prev[next_id] = cur_id;
                    // Recost the heap
                    to_visit.remove(next_node);
                    to_visit.add(next_node);
                }
            }
        }

        // Found a route?
        ArrayList<Integer> route = new ArrayList();
        for (int cur_id = dest_id; cur_id >= 0; cur_id = prev[cur_id]) {
            route.add(cur_id);
        }

        if (route.size() == 1) {
            // This means route not found. src==dest handled by special case above.
            return null;
        }
        else {
            // Convert to array and reverse
            int[] result = new int[route.size()];
            for (int i = 0; i < result.length; i++) {
                result[i] = route.get(result.length - i - 1);
            }
            return result;
        }
    }
}
