package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.Minecart;

import java.util.function.Function;

public abstract class RoutingCondition {
    public enum Type {
        DEFAULT(RoutingConditionDefault::parse),
        EMPTY(RoutingConditionEmpty::parse),
        PLAYER(RoutingConditionPlayer::parse),
        STORAGE(RoutingConditionStorage::parse),
        MOB(RoutingConditionMob::parse),
        DIR(RoutingConditionDir::parse);

        Type(Function<String[], RoutingCondition> clazz) { cond_class = clazz; }
        public final Function<String[], RoutingCondition> cond_class;
    }

    public abstract boolean eval(Minecart cart, CartMeta meta);

    protected CardinalDirection _dir;

    public RoutingCondition(CardinalDirection dir) {
        _dir = dir;
    }

    public CardinalDirection getDir() {
        return _dir;
    }

    public static RoutingCondition fromString(String[] tokens) {
        if (tokens.length < 2) return null;

        Type type = Type.valueOf(tokens[1].toUpperCase());
        return type.cond_class.apply(tokens);
    }

    protected static CardinalDirection parseDirToken(String[] tokens, int pos) {
        if (tokens.length > pos) {
            CardinalDirection direction = CardinalDirection.fromString(tokens[pos]);
            return direction;
        }
        return null;
    }
}
