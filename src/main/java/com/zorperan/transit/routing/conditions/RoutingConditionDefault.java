package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.Minecart;

public class RoutingConditionDefault extends RoutingCondition {
    public RoutingConditionDefault(CardinalDirection dir) {
        super(dir);
    }

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        return true;
    }

    protected static RoutingCondition parse(String[] tokens) {
        CardinalDirection direction = parseDirToken(tokens, 2);
        if (direction != null) {
            return new RoutingConditionDefault(direction);
        }
        return null;
    }
}
