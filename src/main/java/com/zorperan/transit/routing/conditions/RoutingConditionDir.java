package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.Minecart;

public class RoutingConditionDir extends RoutingCondition {
    public RoutingConditionDir(CardinalDirection outDir, CardinalDirection inDir) {
        super(outDir);
        _inDir = inDir;
    }

    private CardinalDirection _inDir;

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        CardinalDirection curDir = CardinalDirection.fromVec(cart.getVelocity());
        return curDir == _inDir;
    }

    protected static RoutingCondition parse(String[] tokens) {
        CardinalDirection outDir = parseDirToken(tokens, 3);
        CardinalDirection inDir = parseDirToken(tokens, 2);
        if (inDir != null && outDir != null) {
            return new RoutingConditionDir(outDir, inDir);
        }
        return null;
    }
}
