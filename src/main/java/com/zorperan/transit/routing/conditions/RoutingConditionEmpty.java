package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Minecart;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class RoutingConditionEmpty extends RoutingCondition {
    public RoutingConditionEmpty(CardinalDirection dir) {
        super(dir);
    }

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        if (cart.getType() == EntityType.MINECART) {
            return cart.getPassengers().isEmpty();
        }

        if (cart instanceof InventoryHolder) {
            Inventory inv = ((InventoryHolder) cart).getInventory();
            for (ItemStack stk : inv.getContents()) {
                if (stk != null) return false;
            }
            return true;
        }

        return false;
    }

    protected static RoutingCondition parse(String[] tokens) {
        CardinalDirection direction = parseDirToken(tokens, 2);
        if (direction != null) {
            return new RoutingConditionEmpty(direction);
        }
        return null;
    }
}
