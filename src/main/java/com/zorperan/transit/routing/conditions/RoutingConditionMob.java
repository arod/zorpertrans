package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Minecart;

public class RoutingConditionMob extends RoutingCondition {
    public RoutingConditionMob(CardinalDirection dir) {
        super(dir);
    }

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        for (Entity e : cart.getPassengers()) {
            if (e instanceof Creature) return true;
        }
        return false;
    }

    protected static RoutingCondition parse(String[] tokens) {
        CardinalDirection direction = parseDirToken(tokens, 2);
        if (direction != null) {
            return new RoutingConditionMob(direction);
        }
        return null;
    }
}
