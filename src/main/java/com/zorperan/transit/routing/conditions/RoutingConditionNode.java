package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.Minecart;

public class RoutingConditionNode extends RoutingCondition {
    public RoutingConditionNode(CardinalDirection dir, int nextId, int cost) {
        super(dir);
        _nextId = nextId;
        _cost = cost;
    }

    private int _nextId;
    private int _cost;

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        if (meta.route != null && meta.route.length > 0) {
            return meta.route[0] == _nextId;
        }
        return false;
    }

    public int getNextId() {
        return _nextId;
    }

    public int getCost() {
        return _cost;
    }

    public static RoutingConditionNode parse(String[] tokens) {
        CardinalDirection direction = parseDirToken(tokens, 3);
        try {
            int nextId = Integer.parseInt(tokens[1]);
            int cost = Integer.parseInt(tokens[2]);
            return new RoutingConditionNode(direction, nextId, cost);
        }
        catch (Exception e) {
            return null;
        }
    }
}
