package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Minecart;

public class RoutingConditionPlayer extends RoutingCondition {
    public RoutingConditionPlayer(CardinalDirection dir) {
        super(dir);
    }

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        for (Entity e : cart.getPassengers()) {
            if (e instanceof HumanEntity) return true;
        }
        return false;
    }

    protected static RoutingCondition parse(String[] tokens) {
        CardinalDirection direction = parseDirToken(tokens, 2);
        if (direction != null) {
            return new RoutingConditionPlayer(direction);
        }
        return null;
    }
}
