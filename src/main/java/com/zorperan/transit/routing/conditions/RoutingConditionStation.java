package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Minecart;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class RoutingConditionStation extends RoutingCondition {
    public RoutingConditionStation(CardinalDirection dir, String station) {
        super(dir);
        _station = station;
    }

    private String _station;

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        return meta.target_station != null && meta.target_station.equals(_station);
    }

    public String getStation() {
        return _station;
    }

    public static RoutingConditionStation parse(String[] tokens) {
        CardinalDirection direction = parseDirToken(tokens, 2);
        String station = tokens.length >= 2 ? tokens[1] : null;
        if (direction != null && station != null && !station.isEmpty()) {
            return new RoutingConditionStation(direction, station);
        }
        return null;
    }
}
