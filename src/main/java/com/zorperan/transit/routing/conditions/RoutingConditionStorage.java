package com.zorperan.transit.routing.conditions;

import com.zorperan.transit.CardinalDirection;
import com.zorperan.transit.CartMeta;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Minecart;

public class RoutingConditionStorage extends RoutingCondition {
    public RoutingConditionStorage(CardinalDirection dir) {
        super(dir);
    }

    @Override
    public boolean eval(Minecart cart, CartMeta meta) {
        return cart.getType() == EntityType.MINECART_CHEST;
    }

    protected static RoutingCondition parse(String[] tokens) {
        CardinalDirection direction = parseDirToken(tokens, 2);
        if (direction != null) {
            return new RoutingConditionStorage(direction);
        }
        return null;
    }
}
