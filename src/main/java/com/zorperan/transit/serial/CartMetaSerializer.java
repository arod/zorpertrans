package com.zorperan.transit.serial;

import com.zorperan.transit.CartMeta;
import com.zorperan.transit.Serializers;
import com.zorperan.transit.ZorperTrans;
import org.bukkit.NamespacedKey;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class CartMetaSerializer
implements PersistentDataType<PersistentDataContainer, CartMeta> {
    public NamespacedKey key;

    private ZorperTrans _plugin;
    private NamespacedKey _keyStorageOfs;
    private NamespacedKey _keyStorageRadius;
    private NamespacedKey _keyTargetStation;
    private NamespacedKey _keyRoute;

    public CartMetaSerializer(ZorperTrans plugin) {
        _plugin = plugin;

        this.key = new NamespacedKey(plugin, "zorp");

        // For sub-keys within this container
        _keyStorageOfs = new NamespacedKey(plugin, "storage_ofs");
        _keyStorageRadius = new NamespacedKey(plugin, "storage_radius");
        _keyTargetStation = new NamespacedKey(plugin, "target_station");
        _keyRoute = new NamespacedKey(plugin, "route");
    }

    @Override
    public Class<PersistentDataContainer> getPrimitiveType() {
        return PersistentDataContainer.class;
    }

    @Override
    public Class<CartMeta> getComplexType() {
        return CartMeta.class;
    }

    @Override
    public PersistentDataContainer toPrimitive(CartMeta complex, PersistentDataAdapterContext context) {
        PersistentDataContainer container = context.newPersistentDataContainer();
        if (complex.storage_ofs != null) {
            container.set(_keyStorageOfs, Serializers.vector, complex.storage_ofs);
        }
        if (complex.storage_radius != null) {
            container.set(_keyStorageRadius, INTEGER, complex.storage_radius);
        }
        if (complex.target_station != null) {
            container.set(_keyTargetStation, STRING, complex.target_station);
        }
        if (complex.route != null) {
            container.set(_keyRoute, INTEGER_ARRAY, complex.route);
        }
        return container;
    }

    @Override
    public CartMeta fromPrimitive(PersistentDataContainer primitive, PersistentDataAdapterContext context) {
        CartMeta result = new CartMeta();

        result.storage_ofs = primitive.get(_keyStorageOfs, Serializers.vector);
        result.storage_radius = primitive.get(_keyStorageRadius, INTEGER);
        result.target_station = primitive.get(_keyTargetStation, STRING);
        result.route = primitive.get(_keyRoute, INTEGER_ARRAY);

        return result;
    }
}
