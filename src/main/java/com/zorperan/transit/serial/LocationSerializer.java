package com.zorperan.transit.serial;

import com.zorperan.transit.Serializers;
import com.zorperan.transit.ZorperTrans;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class LocationSerializer implements PersistentDataType<PersistentDataContainer, Location> {
    Server _server;
    NamespacedKey _posTag;
    NamespacedKey _worldTag;

    public LocationSerializer(ZorperTrans plugin) {
        _server = plugin.getServer();
        _posTag = new NamespacedKey(plugin, "pos");
        _worldTag = new NamespacedKey(plugin, "world");
    }

    @Override
    public Class<PersistentDataContainer> getPrimitiveType() {
        return PersistentDataContainer.class;
    }

    @Override
    public Class<Location> getComplexType() {
        return Location.class;
    }

    @Override
    public PersistentDataContainer toPrimitive(Location complex, PersistentDataAdapterContext context) {
        PersistentDataContainer result = context.newPersistentDataContainer();
        result.set(_posTag, Serializers.vector, complex.toVector());
        result.set(_worldTag, STRING, complex.getWorld().getName());
        return result;
    }

    @Override
    public Location fromPrimitive(PersistentDataContainer primitive, PersistentDataAdapterContext context) {
        int[] pos = primitive.get(_posTag, INTEGER_ARRAY);
        String world_str = primitive.get(_worldTag, STRING);
        World world = _server.getWorld(world_str);

        Location result = new Location(world, pos[0], pos[1], pos[2]);
        return result;
    }
}
