package com.zorperan.transit.serial;

import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;

public class VectorSerializer implements PersistentDataType<int[], Vector> {
    @Override
    public Class<int[]> getPrimitiveType() {
        return int[].class;
    }

    @Override
    public Class<Vector> getComplexType() {
        return Vector.class;
    }

    @Override
    public int[] toPrimitive(Vector complex, PersistentDataAdapterContext context) {
        int[] pos = {complex.getBlockX(), complex.getBlockY(), complex.getBlockZ()};
        return pos;
    }

    @Override
    public Vector fromPrimitive(int[] primitive, PersistentDataAdapterContext context) {
        return new Vector(primitive[0], primitive[1], primitive[2]);
    }
}
